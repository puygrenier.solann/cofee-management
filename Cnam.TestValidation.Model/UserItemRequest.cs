﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum RequestStatus
{
    InProgress,
    Validated,
    Canceled
}

namespace Cnam.TestValidation.Model
{
    public class UserItemRequest : IEquatable<UserItemRequest>
    {
        public Guid Guid { get; set; }
        public User User{ get; set; }
        public Item Item { get; set; }
        public int RequestedQuantity { get; set; }
        public RequestStatus RequestStatus { get; set; }

        public UserItemRequest(Guid guid,int RequestedQuantity, RequestStatus RequestStatus) 
        {
            Guid = guid;
            this.RequestedQuantity = RequestedQuantity;
            this.RequestStatus = RequestStatus;
        }
        public UserItemRequest(User user, Item item, int quantity) : this(GenerateId(),user, item, quantity, RequestStatus.InProgress) { }
        public UserItemRequest(Guid id, User user, Item item, int quantity,RequestStatus status )
        {
            User = user;
            Item = item;
            RequestedQuantity = quantity;
            RequestStatus = status;
            Guid = id;
        }

        public decimal GetCostOfRequest()
        {
            return RequestedQuantity * Item.Price;
        }

        private static Guid GenerateId()
        {
            return Guid.NewGuid();
        }

        public bool Equals(UserItemRequest? other)
        {
            return this is not null
                && other is not null
                && this.User.Equals(other.User)
                && this.RequestedQuantity.Equals(other.RequestedQuantity)
                && this.RequestStatus.Equals(other.RequestStatus)
                && this.Guid.Equals(other.Guid)
                && this.Item.Equals(other.Item);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
