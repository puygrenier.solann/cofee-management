﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cnam.TestValidation.Model
{
    public class Item : IEquatable<Item>
    {
        public Guid Id { get; set; }
        public string EAN { get; set; } //id
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }

        public Item(){}
        public Item(Guid id,string ean, string title,decimal price,string description)
        {
            if (Decimal.Round(price, 2) != price) throw new ArgumentOutOfRangeException("Price value cannot have more than 2 digits place");
            if (price <= 0) throw new ArgumentOutOfRangeException("Item price cannot be real (zero or negativ)");
            Id = id;
            Title = title;
            Price = price;
            Description = description;
            EAN = ean;
        }

        public Item(string ean, string title, decimal price, string description) : this(Guid.NewGuid(), ean, title, price, description) { }

        public bool Equals(Item? other)
        {
            if (other is null && this is null) return true;
            else return Id.Equals(other.Id)
                || (this.EAN.Equals(other.EAN) && this.Title.Equals(other.Title));
                /*&& Price.Equals(other.Price) 
                && Description.Equals(other.Description);*/
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
