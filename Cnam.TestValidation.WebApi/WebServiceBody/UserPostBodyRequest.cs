﻿using Cnam.TestValidation.Model;
using Newtonsoft.Json;


namespace Cnam.TestValidation.WebApi.WebServiceBody
{
#pragma warning disable CS1591 // Commentaire XML manquant pour le type ou le membre visible publiquement
    public class UserPostBodyRequest
    {
        /// <summary>
        /// Description for Swagger
        /// </summary>
        /// <example>kevinDu69</example>
        public string UserName { get; set; }

        /// <summary>
        /// Description for Swagger
        /// </summary>
        /// <example>pip0du69</example>
        public string Password { get; set; }

        public static implicit operator User(UserPostBodyRequest user) {
            return new User(user.UserName,user.Password);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
#pragma warning restore CS1591 // Commentaire XML manquant pour le type ou le membre visible publiquement
}
