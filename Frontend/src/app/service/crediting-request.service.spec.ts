import { TestBed } from '@angular/core/testing';

import { CreditingRequestService } from './crediting-request.service';

describe('CreditingRequestService', () => {
  let service: CreditingRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreditingRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
