import { Component, OnInit , Input} from '@angular/core';
import { UserModel } from '../model/user-model.model';

import { UserPurseService } from '../service/user-purse.service'
import { PurseUpdateService } from '../service/purse-update.service'

const  responeToRequest = {
  200: "Succefull update",
  400: "Error are in the request",
  404: "User not found",
  409: "Cannot decrease because of lacking fund",
  500: "Something went wrong on the server side"
}

@Component({
  selector: 'app-users-summary',
  templateUrl: './users-summary.component.html',
  styleUrls: ['./users-summary.component.css']
})

export class UsersSummaryComponent implements OnInit {
  public value:number = 0;
  public submitingInprgoress:boolean = false;
  @Input() userSummary: UserModel = new UserModel("a",0);

  constructor(private purseUpdaterService : PurseUpdateService, private purseService:UserPurseService) { }

  ngOnInit(): void {
  }


  SetPlusAction(){
    this.value++
  }
  SetMinusAction(){
    this.value--
  }

  ExecuteUpdate(){
    this.submitingInprgoress = true;

    if(this.value > 0) this.purseUpdaterService.IncreasePurseOfUser(this.userSummary.id,this.value).subscribe(
      (response) => { this.purseService.GetPurseValueWithId(this.userSummary.id).subscribe((response) => { this.userSummary.purse = response})},
      (not200Response) => {this.submitingInprgoress = false},
      () => {this.submitingInprgoress = false})

    else if (this.value < 0 ) this.purseUpdaterService.DecreasePurseOfUser(this.userSummary.id,Math.abs(this.value)).subscribe(
      (response) => { this.purseService.GetPurseValueWithId(this.userSummary.id).subscribe((response) => { this.userSummary.purse = response})},
      (not200Response) => {this.submitingInprgoress = false},
      () => {this.submitingInprgoress = false})


    this.value = 0;

  }

}
