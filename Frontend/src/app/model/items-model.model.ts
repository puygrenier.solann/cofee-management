import { Injectable } from "@angular/core";
import { AdapterItem } from "../interface/adapter-item";

export class Item {

       constructor(public title: string,
        public price: string,
        public id:string,
        public description:string)
        {


        }

}
@Injectable({
    providedIn: "root",
  })
  export class ItemsAdapter implements AdapterItem<Item> {
    adapt(i: any): Item {
    return new Item(i.title, i.price, i.id, i.description);
    }
  }
