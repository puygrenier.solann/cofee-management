﻿using Cnam.TestValidation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Infrastructure
{
    public interface IDataExchange
    {
        public bool CreateAnUser(User user);
        public User GetAnUser(Guid guid);
        public bool DeleteAnUser(User user);
        public bool CreateItem(Item item);
        public Item? GetItem(Item item);
        public Item? GetItem(Guid item);
        public bool DeleteItem(Item item);
        public bool UpdateItem(Item item);
        public List<Item> GetAllItem();
        public List<User> GetAllUser();
        public bool IncreaseUserPurse(User user, decimal valueToAdd);
        public bool DecreseUserPurse(User user, decimal valueToRemove);
        public bool CreateItemUserRequest(UserItemRequest request);
        public UserItemRequest GetItemUserRequest(UserItemRequest request);
        public UserItemRequest GetItemUserRequest(Guid guid);
        public List<UserItemRequest> GetListItemUserRequest(RequestStatus status);
        bool UserAccesWithCorrectValue(string name, string password);
        Guid? GetGuidUser(string name, string password);
        public List<UserItemRequest> GetListItemUserRequest(User user);
        public bool CancelItemUserRequest(UserItemRequest request);
        public bool ValidatedItemUserRequest(UserItemRequest request);
        public bool DeleteItemUserRequest(UserItemRequest request);


    }
}
