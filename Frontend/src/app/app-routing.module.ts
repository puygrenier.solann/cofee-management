import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AccountComponent } from './account/account.component';
import { ItemsComponent } from "./items/items.component";
import { BasketComponent } from './basket/basket.component';
import { AdminComponent } from "./admin/admin.component";
const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'basket', component: BasketComponent},
  { path: 'main', component: ItemsComponent},
  { path: 'account', component: AccountComponent},
  { path: 'admin', component:AdminComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
