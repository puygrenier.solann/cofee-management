import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { AdapterRequest } from '../interface/adapter-request';
import { UserRequest, UserRequestAdapter } from '../model/user-request.model';
import { map, Observable, Subject } from 'rxjs';
const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*")
            .set('Content-Type','application/json');

@Injectable({
  providedIn: 'root'
})
export class UserRequestService {

  public SubjectProducts = new Subject<UserRequest[]>();
  SubjectProducts$: any;
  public request: UserRequest []= [];
  constructor(private http: HttpClient, private adapter: UserRequestAdapter) { }

  GetUserRequest():Observable<UserRequest[]>{
    const idUser= window.sessionStorage.getItem("token");
    const url="https://localhost:7210/api/CofeeManagerRequest/request/user/"+idUser;
    console.log(url);
    return this.http.get(url).pipe(
        map((data: any) => data.map((request: any) => this.adapter.adapt(request)))
      )
    //return this.http
      //.get<any>(url,{
        //headers: headers

        //params: params,
      //});//.subscribe((res) => console.log(res));
  }
}
