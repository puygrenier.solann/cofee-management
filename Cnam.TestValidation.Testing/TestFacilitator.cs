﻿using Cnam.TestValidation.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Testing
{
    public static class TestFacilitator
    {
        public static Item RandomItemGenerator()
        {
            return RandomItemGenerator(ProvideRandomInt());
        }
        public static Item RandomItemGenerator(decimal PriceOfItem)
        {
            string ean = Path.GetRandomFileName();
            string randomTitle = Path.GetRandomFileName();
            string randomDescription = Path.GetRandomFileName();
            decimal randomPrice = PriceOfItem;
            Item ItemToUpdate = new Item(ean, randomTitle, randomPrice, randomDescription);
            return ItemToUpdate;
        }

        public static User RandomUserGenerator()
        {
            string username = Path.GetRandomFileName();
            string password = Path.GetRandomFileName();
            User user = new User(username, password);
            return user;
        }
        public static decimal ProvideRandomPrice()
        {
            Random random = new Random();
            decimal decimalValue = Convert.ToDecimal(random.NextDouble()) * 10 + Convert.ToDecimal(random.NextDouble());
            return Decimal.Round(decimalValue, 2);
        }
        public static int ProvideRandomInt()
        {
            Random random = new Random();
            return (int)random.NextInt64(1, 100);
        }
    }
}
