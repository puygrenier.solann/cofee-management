﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.DomainService.CofeeUseCaseException
{
    [Serializable]
    public class CofeeUseCaseItemCreationException : Exception
    {
        public CofeeUseCaseItemCreationException() : base() { }
        public CofeeUseCaseItemCreationException(string? message) : base(message) { }

        public CofeeUseCaseItemCreationException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected CofeeUseCaseItemCreationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
