import { Item } from "../model/items-model.model";

export class AddItem {
  static readonly type = '[Item] Add';

  constructor(public payload: Item) {}
}

export class DeleteItem {
    static readonly type = '[Item] Delete';
  
    constructor(public payload: Item) {}
  }