import { Injectable } from "@angular/core";
import { Item } from './items-model.model';
import { AdaptaterAdminRequest } from "../interface/adaptater-admin-request";

import { UserRequest } from '../model/user-request.model'
export enum Status {
    InProgress = 'InProgress',
    Validated = 'Validated',
    Canceled = 'Canceled',
    Undefined = '?'
  }

export class AdminRequest {
  constructor(
    public id:string,
    public userName:string,
    public item:Item,
    public quantity:number,
    public status:Status
  ){}
  convertToUserRequest(): UserRequest{
    return new UserRequest(this.id,this.item,this.quantity,this.status)
  }
}

@Injectable({
    providedIn: "root",
  })
  export class AdminRequestAdaptater implements AdaptaterAdminRequest<AdminRequest>  {
    adapt(i: any): AdminRequest {
    return new AdminRequest(i.guid, i.user.userName,i.item,i.requestedQuantity,i.requestStatus);
    }
  }
