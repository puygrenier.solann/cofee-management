import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemsComponent } from './items/items.component';
import { HttpClientModule } from '@angular/common/http';
import { ItemsServiceService } from './service/items-service.service';
import { BasketComponent } from './basket/basket.component';
import { ItemState } from './state/item-state';
import { NgxsModule } from '@ngxs/store';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { AccountComponent } from './account/account.component';
import { HeaderComponent } from './header/header.component';
import { ItemSummaryComponent } from './item-summary/item-summary.component';
import { AdminComponent } from './admin/admin.component';
import { RequestSummaryComponent } from './request-summary/request-summary.component';
import { UsersComponent } from './users/users.component';

import { RequestComponent } from './request/request.component';
import { UsersSummaryComponent } from './users-summary/users-summary.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    AppComponent,
    ItemsComponent,
    BasketComponent,
    FooterComponent,
    LoginComponent,
    AccountComponent,
    HeaderComponent,
    ItemSummaryComponent,
    AdminComponent,
    RequestSummaryComponent,
    UsersComponent,
    RequestComponent,
    UsersSummaryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    NgxsModule.forRoot([ItemState]),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
