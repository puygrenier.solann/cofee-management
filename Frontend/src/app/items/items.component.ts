import { Component, OnInit } from '@angular/core';
import { ItemsServiceService } from '../service/items-service.service';
import { Item } from '../model/items-model.model';
import { ItemSummaryComponent } from '../item-summary/item-summary.component';
import { DataHeaderService } from "../service/data-header.service";
import { Router } from "@angular/router";

import { AddItem } from '../actions/item-action';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngxs/store';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  countryData: Object | undefined;
  items: Item[];
  ProductList: Item[]= [];
  searchText: any;
  Products: Subscription | undefined;
  constructor(private itemsService: ItemsServiceService,private store:Store,private router: Router) {
    this.items = [];
  }

  ngOnInit(): void {
    this.itemsService.list().subscribe((items: Item[]) => {this.items = items;});
    if( ! window.sessionStorage.getItem("token") ) this.router.navigate(['/']);
  }

  addItem(item: Item) {
    this.store.dispatch(new AddItem(item)); 
    console.log(item);

  }
}
