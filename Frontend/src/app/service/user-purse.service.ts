import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from "@angular/common/http";

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*")
            .set('Content-Type','application/json');

@Injectable({
  providedIn: 'root'
})
export class UserPurseService {

  constructor(private http: HttpClient) { }

  GetPurseValue(){
    const idUser= window.sessionStorage.getItem("token");
    const url="https://localhost:7210/api/CofeeManagerUser/user/"+idUser+"/purse";
    console.log(url);
    return this.http
      .get<any>(url,{
        headers: headers
        //params: params,
      });//.subscribe((res) => console.log(res));
  }
  GetPurseValueWithId(id:string){
    const url="https://localhost:7210/api/CofeeManagerUser/user/"+id+"/purse";
    console.log(url);
    return this.http
      .get<any>(url,{
        headers: headers
        //params: params,
      });//.subscribe((res) => console.log(res));
  }

}
