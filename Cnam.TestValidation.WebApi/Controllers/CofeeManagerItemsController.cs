﻿using Cnam.TestValidation.DomainService;
using Cnam.TestValidation.DomainService.CofeeUseCaseException;
using Cnam.TestValidation.Infrastructure;
using Cnam.TestValidation.Model;
using Cnam.TestValidation.WebApi.WebServiceBody;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Cnam.TestValidation.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CofeeManagerItemsController : ControllerBase
    {
        private readonly ILogger<CofeeManagerUserController> _logger;
        public IDataExchange postGreInfrastructure = new PostgreInfrastructure();
        public CofeeManagerItemsController(ILogger<CofeeManagerUserController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Allow to create new item
        /// </summary>
        /// <remarks>Can be used for the creation of a new item</remarks>
        /// <response code="201">Successfull creation of item</response>
        /// <response code="409">Conflict because of already created ressources</response>
        /// <response code="400">Request is not correctly formated</response>
        [HttpPost]
        [Route("item")]
        public IActionResult CreateItem(ItemPostBodyRequest item)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);

            if (item.IsIncomplete()) return BadRequest();
            try
            {
                cofeeUseCase.AddItem(item);
                return Created("", "SUCCES");
            }
            catch (CofeeUseCaseItemCreationException ex)
            {
                return Conflict(JsonConvert.SerializeObject(ex));
            }
            catch (Exception ex)
            {
                _logger.LogError(JsonConvert.SerializeObject(ex));
                var result = Content(JsonConvert.SerializeObject(ex), "application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        /// <summary>
        /// Provide data regarding one existing item
        /// </summary>
        /// <remarks>Can be used for the acces of a new item</remarks>
        /// <response code="200">Item found</response>
        /// <response code="404">Item not found</response>
        /// <response code="500">Something went wrong</response>
        [HttpGet]
        [Route("item/{itemId}")]
        public IActionResult GetItem([FromRoute] Guid itemId)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            Item targetItem;
            try
            {
                targetItem = cofeeUseCase.GetItem(itemId);
                return Ok(targetItem);
            }
            catch (CofeeUseCaseItemAccesException ex)
            {
                return NotFound(JsonConvert.SerializeObject(ex));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                var result = Content(JsonConvert.SerializeObject(ex), "application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }


        /// <summary>
        /// Allow to update item data
        /// </summary>
        /// <remarks>Can be used for item modification</remarks>
        /// <response code="200">Item correctly updated</response>
        /// <response code="404">No item found</response>
        /// <response code="500">Something went wrong</response>
        [HttpPut]
        [Route("item")]
        public IActionResult PutItem(ItemPutBodyRequest item)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                cofeeUseCase.UpdateItem(item);
                return Ok("SUCCES");
            }
            catch (CofeeUseCaseItemAccesException ex)
            {
                return NotFound(JsonConvert.SerializeObject(ex));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                var result = Content(JsonConvert.SerializeObject(ex), "application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
            
        }

        /// <summary>
        /// Allow to remove item
        /// </summary>
        /// <remarks>Can be used for deleting item </remarks>
        /// <response code="200">Item correctly removed</response>
        /// <response code="404">No item found</response>
        /// <response code="500">Something went wrong</response>
        [HttpDelete]
        [Route("item/{idItem}")]
        public IActionResult DeleteItem([FromRoute] Guid idItem)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                Item targetItem = cofeeUseCase.GetItem(idItem);
                cofeeUseCase.DeleteItem(targetItem);
                return Ok("SUCCES");
            }
            catch (CofeeUseCaseItemAccesException ex)
            {
                return NotFound(JsonConvert.SerializeObject(ex));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                var result = Content(JsonConvert.SerializeObject(ex), "application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        /// <summary>
        /// Provide data regarding all existing item
        /// </summary>
        /// <response code="200">The list of all item</returns>
        /// <response code="500">Something went wrong</response>
        [HttpGet]
        [Route("item/all")]
        public IActionResult ListAlItem()
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                return Ok(cofeeUseCase.GetAllItem());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                var result = Content(JsonConvert.SerializeObject(ex), "application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }
    }

}

