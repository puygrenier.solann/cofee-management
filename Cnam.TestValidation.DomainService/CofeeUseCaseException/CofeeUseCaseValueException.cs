﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.DomainService.CofeeUseCaseException
{
    [Serializable]
    public class CofeeUseCaseValueException : GenericUseCaseException
    {
        public CofeeUseCaseValueException(string? message) : base(message)
        {
        }
    }
}
