﻿using Cnam.TestValidation.DomainService;
using Cnam.TestValidation.Infrastructure;
using Cnam.TestValidation.Model;
using System.Text.Json;

namespace Cnam.TestValidation.ApplicationService
{
    public class ApplicationMediator : IMediatorUseCase
    {
        private CofeeUseCase cofeeUseCase;
        private IDataExchange infrastructure;

        public ApplicationMediator(){}
        public void SetInfra(IDataExchange infra)
        {
            infrastructure = infra;
        }
        public void SetUseCase(CofeeUseCase cofee) 
        {
            cofeeUseCase = cofee;
        }

        public void NotifyMediatorForInsertUserData(User createdUser)
        {
            infrastructure.CreateAnUser(createdUser);
        }

        // IN PROGRESS

        public void NotifyMediatorForAskingLoadingItems(CofeeUseCase cofeeUseCase)
        {
            //infrastructure.
        }

        public void NotifyMediatorForAskingLoadingData(IDataExchange infra)
        {
            //cofeeUseCase.Load(infra.GetData());
        }
    }
}