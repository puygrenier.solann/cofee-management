﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.DomainService.CofeeUseCaseException
{
    [Serializable]
    public class GenericUseCaseException : Exception
    {
        public GenericUseCaseException()
        {
        }
        public GenericUseCaseException(string? message) : base(message)
        {
        }
        public GenericUseCaseException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
        protected GenericUseCaseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
