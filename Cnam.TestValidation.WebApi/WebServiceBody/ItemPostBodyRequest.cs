﻿using Cnam.TestValidation.Model;

namespace Cnam.TestValidation.WebApi.WebServiceBody
{
    public class ItemPostBodyRequest
    {
        /// <summary>
        /// Swager description
        /// </summary>
        /// <example>5478000267412</example>
        public string EAN { get; set; }
        /// <summary>
        /// Swager description
        /// </summary>
        /// <example>Coca-Cola</example>
        public string Title { get; set; }

        /// <summary>
        /// Swager description
        /// </summary>
        /// <example>1.50</example>
        public decimal Price { get; set; }
        /// <summary>
        /// Swager description
        /// </summary>
        /// <example>Boisson gazeuse célèbre</example>
        public string Description { get; set; }

        public static implicit operator Item(ItemPostBodyRequest item){
            return new Item(item.EAN,item.Title, item.Price, item.Description);
        }

        public bool IsIncomplete()
        {
            return (EAN is null) || (Title is null) || (Price is 0) || (Description is null);
        }
    }
}
