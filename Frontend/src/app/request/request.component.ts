import { Component, OnInit } from '@angular/core';
import { RequestAdminService } from '../service/request-admin.service';
import { Status, AdminRequest } from '../model/admin-request.model';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  public allRequest:AdminRequest[] = [];
  public statusChoice: Status = Status.InProgress;
  public isInProgressSearch:boolean = true;
  constructor(private accesRequest : RequestAdminService) { }

  ngOnInit(): void {
      this.accesRequest.AccesAllRequestByStatus(this.statusChoice).subscribe(
        (allRequest: AdminRequest[]) => {this.allRequest = allRequest;}
      )
  }

  public SearchInProgress(){
      this.statusChoice = Status.InProgress;
      this.isInProgressSearch = true;
      this.Research();
  }

  public SearchValidated(){
    this.statusChoice = Status.Validated;
    this.isInProgressSearch = false;
      this.Research();
  }

  public searchCanceled(){
    this.statusChoice = Status.Canceled;
    this.isInProgressSearch = false;
      this.Research();
  }

  public Research(){
    this.accesRequest.AccesAllRequestByStatus(this.statusChoice).subscribe(
      ((allRequest: AdminRequest[]) => {this.allRequest = allRequest;})
    )
  }
}
