import { Component, OnInit } from '@angular/core';
import { UserCreationService } from '../service/user-creation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  public login:string;
  public password:string;
  public errorMessage:string;
  public isError:boolean=false;
  public stillNotCreated:boolean=true;

  constructor(private creationService:UserCreationService,private router: Router) {
    this.login="";
    this.password="";
    this.errorMessage="";
   }


  TryCreation(){
    this.creationService.AccountCreation(this.login,  this.password).subscribe(
      (response) => this.stillNotCreated = false,
      (not200response) => {
        if (not200response.status == 201) this.stillNotCreated = false;
        else if (not200response.status == 409) {
          this.isError = true;
          this.errorMessage = "Please try another login"
        }


      },
      () => console.log("finished"));
  }
  ngOnInit(): void {
  }

}
