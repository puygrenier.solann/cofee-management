import { Component, OnInit } from '@angular/core';
import { UserListingService } from '../service/user-listing.service'
import { UserModel } from '../model/user-model.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public allUsers: UserModel[] = []
  constructor(private listingService:UserListingService) { }

  ngOnInit(): void {
    this.listingService.GetAllUser().subscribe(
      (allUsers: UserModel[]) => {this.allUsers = allUsers;})
  }

}
