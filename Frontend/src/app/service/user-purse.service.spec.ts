import { TestBed } from '@angular/core/testing';

import { UserPurseService } from './user-purse.service';

describe('UserPurseService', () => {
  let service: UserPurseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserPurseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
