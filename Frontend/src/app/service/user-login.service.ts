import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { UserModel, UserAdapter } from '../model/user-model.model';
import { Observable } from 'rxjs';

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*")
            .set('Content-Type','application/json');
@Injectable({
  providedIn: 'root'
})
export class UserLoginService {


  constructor(
    private http: HttpClient,
    private adapter: UserAdapter ){};

  Authentification(login:string,password:string): Observable<string> {
    const url="https://localhost:7210/api/CofeeManagerUser/user/token";
    var body = {
                  "userName": login,
                  "password": password,
                };
    return this.http
      .post<any>(url, body, {
        headers: headers
        //params: params,
      });//.subscribe((res) => console.log(res));
  }

  IsAdminAuthentification(login:string,password:string): boolean{
    return (login === 'admin' && password =='admin' )
  }
}
